#include <fstream>
#include <cstring>
#include <string>
#include <iostream>

#include "File_Reader.h"

using namespace std;

times convertTime(char* str)
{
    times result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.min = atoi(str_number);
    return result;
}



void read(const char* file_name, report* array[], int& size) {

    std::ifstream file(file_name);
    if (file.is_open())
    {
        int i = 0;
        char tmp_buffer[MAX_STRING_SIZE];


        while (!file.eof()) {

            report* item = new report;

            file >> tmp_buffer;
            item->start = convertTime(tmp_buffer);
            file >> tmp_buffer;
            item->end = convertTime(tmp_buffer);
            file >> item->name.surname;
            file >> item->name.name;
            file >> item->name.patr;
            file.ignore();
            file.getline(item->topic, MAX_STRING_SIZE);

            array[i] = item;
            i++;
        }
        size = i;
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}