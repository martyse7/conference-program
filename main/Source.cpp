#include "constans.h"
#include "report.h"
#include "File_Reader.h"
#include "output.h"
#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #2. Conference\n";
    cout << "Author: ...\n";
    cout << "Group: 16\n";


    report* array[MAX_FILE_ROWS_COUNT];
    int size;

    try
    {

        read("data.txt", array, size);

        for (int i = 0; i < size; i++) {
            output(array[i]);
        }

    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
}