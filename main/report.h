#ifndef REPORT_H
#define REPORT_H

#include "constans.h"

struct names {
    char name[MAX_STRING_SIZE];
    char surname[MAX_STRING_SIZE];
    char patr[MAX_STRING_SIZE];
  
};
    struct times {
    int min;
    int hour;
};

struct report {
    times start;
    times end;
    names name;
    char topic[MAX_STRING_SIZE];
   
};
#endif