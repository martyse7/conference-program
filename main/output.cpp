#include "output.h"
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

void output(report* array) {

	cout << array->start.hour << ".";
	if (array->start.min < 10) cout << "0";
	cout << array->start.min << " ";
	cout << array->end.hour << ".";
	if (array->end.min < 10) cout << "0";
	cout << array->end.min << " ";
	cout << array->name.surname << " ";
	cout << array->name.name << " ";
	cout << array->name.patr << " ";
	cout << array->topic << " ";
	
	cout << '\n';

}